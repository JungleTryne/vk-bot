from bot import Bot

def main():
    print('Запускаем бота...')
    my_bot = Bot()
    print('Бот запущен')
    my_bot.longpoll_listening()

if __name__ == "__main__":
    main()