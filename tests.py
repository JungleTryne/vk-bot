import unittest
from bot import MoppingHandler

test_case_1 = '4\n145675539\n210445124\n227959395\n296350441\n0\n0\n0\n'


def prepare_file(test_case):
    with open('Mopping.txt', 'w+') as file:
            file.write(test_case)


class TestMoppingHandler(unittest.TestCase):


    def test_reading(self):
        prepare_file(test_case_1)
        mopping_handler = MoppingHandler()

        self.assertEqual(mopping_handler.voted_for, [])
        self.assertEqual(mopping_handler.voted_against, [])
        self.assertFalse(mopping_handler.is_poll_started)
        self.assertEqual(mopping_handler.queue_size, 4)
        self.assertEqual(mopping_handler.queue, [145675539, 210445124, 227959395, 296350441])


    def test_save(self):
        prepare_file(test_case_1)
        
        mopping_handler = MoppingHandler()
        mopping_handler.save_to_file()

        with open('Mopping.txt', 'r') as file:
            self.assertEqual(file.read(), '4\n145675539\n210445124\n227959395\n296350441\n0\n0\n0\n')


    def test_next_in_queue(self):
        prepare_file(test_case_1)
        mopping_handler = MoppingHandler()
        self.assertEqual(296350441, mopping_handler.who_is_next())
        mopping_handler.go_further()
        self.assertEqual(227959395, mopping_handler.who_is_next())


if __name__ == '__main__':
    unittest.main()
#to be continued
