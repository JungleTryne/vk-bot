import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from keys import _vk_token
import random

import threading
import time

from datetime import datetime

import logging
import phrases
# ID Пользователей:
# 210445124 - Данила Мишин
# 145675539 - Миша Коваль
# 227959395 - Вова Макаров
# 296350441 - Леша Солодов

# 227959395 145675539 210445124 296350441
#
#
#

class Bot:
    """ Основной класс для обработки запросов ботом

    :param vk:  Основной объект библиотеки vk_api. 
        Используется для получения нужного функционала

    :param longpoll: Объект для использования LongPoll Callback API

    :param vk_api: Объект, позволяющий использовать интерфейс VK API

    :param messages_handler: Обработчик сообщений
    """

    def __init__(self) -> None:
        self.vk = vk_api.VkApi(token=_vk_token)
        self.longpoll = VkLongPoll(self.vk)
        self.vk_api = self.vk.get_api()
        self.messages_handler = MessageHandler(self.vk_api)
        self.notification_handler = NotificationsHandler(self.vk_api)

    def longpoll_listening(self) -> None:
        """ Стартер прослушки запросов боту
        """
        print('Начали слушать сервачок')
        for event in self.longpoll.listen():
            if event.type == VkEventType.MESSAGE_NEW:
                if event.to_me:
                    self.messages_handler.handle_request(event)

    def notification_scheduler(self) -> None:
        """ Стартер отправки уведомлений
        """
        pass


class VkEssentials:
    """Класс, упрощающий работу с VK API

    :param vk_api: Объект, позволяющий использовать интерфейс VK API
    """

    def __init__(self, vk_api):
        self.vk_api = vk_api

    def get_user_full_name(self, vk_id) -> tuple:
        """Получение ФИ пользователя

        :param vk_id: id пользователя ВК 
        """
        user_info = self.vk_api.users.get(user_ids=vk_id)
        return (user_info[0]['first_name'], user_info[0]['last_name'])

    def send_message(self, user_id, message,
                     keyboard=VkKeyboard.get_empty_keyboard()):
        """Функция отправки сообщения

        :param user_id: Vk ID пользователя, которому отправляем сообщение

        :param message: Сообщение, которое отправляем

        """
        print('[{0}] Отправляем пользователю {1} сообщение \"{2}\"'
              .format(str(datetime.now())[:-7], user_id, message))

        self.vk_api.messages.send(user_id=user_id,
                                  message=message,
                                  random_id=random.randint(0, 2**64-1),
                                  keyboard=keyboard)

    def prepare_keyboard(self, user_id):
        """Функция, которая генерирует клавиатуру для пользователя

        :param user_id: ID пользователя VK

        """

        mops_class = MoppingHandler()

        keyboard = VkKeyboard()
        keyboard.add_button('/who')
        keyboard.add_button('/queue')
        if int(user_id) == mops_class.who_is_next():
            if not mops_class.poll_started():
                keyboard.add_button('/finished',
                                    color=VkKeyboardColor.PRIMARY)

        elif (mops_class.poll_started()) and \
                ((not int(user_id) in mops_class.get_users_for()) and
                    (not int(user_id) in mops_class.get_users_against())):
            keyboard.add_button('/accept', color=VkKeyboardColor.POSITIVE)
            keyboard.add_button('/decline', color=VkKeyboardColor.NEGATIVE)

        return keyboard.get_keyboard()


class NotificationsHandler:
    """ Основной класс для работы с уведомлениями
    Так как создатели vk_api не в курсе, что такое 
    асинхронное программирование, нам придётся все время создавать 
    потоки для новых функций

    Пример такой функции посмотрите notifications.py. 
    Файл не является рабочим, служит пока что как пример, скоро будет удален

    :param essential: инструменты для отправки сообщений
    :param scheduled_notifications: список функций (!), 
        которые обрабатывают уведомления

    """

    def __init__(self, vk_api):
        self.essential = VkEssentials(vk_api)

        self.scheduled_notifications = [
            self.mopping_notification
        ]

        for notification in self.scheduled_notifications:
            notification()

    def mopping_notification(self):
        """Функция отправки уведомлений-напоминаний о том, что надо
        мыть полы

        :param vk_api: - объект API
        """
        print('Проверяем время для отправки сообщения')

        if datetime.now().hour >= 12 and datetime.now().hour <= 13:
            print('Отправлено напоминание о мытье полов')

            mops_class = MoppingHandler()
            user_id= mops_class.who_is_next()

            self.essential.send_message(
                user_id, 
                'Не забудь помыть полы',
                self.essential.prepare_keyboard(user_id),
            )

        threading.Timer(3600, self.mopping_notification).start()
        


class MessageHandler:
    """ Основной класс для работы с сообщениями
    :param vk_api: Объект, позволяющий использовать интерфейс VK API

    :param essential: Объект класса VkEssentials

    :param command_list: Каждой команде сопоставляется функция, которая 
        вызывается для обработки
    """

    def __init__(self, vk_api):
        self.vk_api = vk_api
        self.essential = VkEssentials(vk_api)
        self.admin = AdminPanel()
        self.commands_list = {
            '/who': self.who_mops,
            '/finished': self.finished,
            '/accept': self.accept,
            '/decline': self.decline,
            '/reset': self.reset,
            '/queue': self.get_queue
        }

    def handle_request(self, request) -> None:
        """Функция вызова нужно функции для обработки запроса

        :param request: Объект запроса

        """
        log_message = phrases.message_got
        print(log_message.format(
            str(datetime.now())[:-7],
            request.user_id, request.message))
        if not request.message.split()[0] in self.commands_list:
            self.essential.send_message(request.user_id,
                                        phrases.message_unknown_command,
                                        self.essential.prepare_keyboard(request.user_id)
                                        )
        else:
            self.commands_list[request.message.split()[0]](request)

    def who_mops(self, request):
        """Функция, которая запускается при команде 
            /who (Кто моет пол следующий)

        :param request: Объект запроса

        """
        mops_class = MoppingHandler()
        mopping_person_id = mops_class.who_is_next()
        message = phrases\
            .message_next\
            .format(*self.essential
                    .get_user_full_name(mopping_person_id)
                    )

        self.essential.send_message(request.user_id,
                                    message,
                                    self.essential.prepare_keyboard(request.user_id))

    def finished(self, request):
        """Функция, которая запускается при команде /finished
            (команду запускает человек, который помыл полы)

        :param request: Объект запроса

        """
        mops_class = MoppingHandler()
        if (not mops_class.poll_started()
                and mops_class.is_mopping(request.user_id)):

            mops_class.start_poll()
            for user in mops_class.get_users():
                if not user == request.user_id:
                    message = phrases.message_wow\
                        .format(*self.essential.get_user_full_name(request.user_id)),
                    self.essential.send_message(user,
                                                message,
                                                self.essential.prepare_keyboard(user))

            message = phrases.message_okay_boomer
            self.essential.send_message(request.user_id,
                                        message,
                                        self.essential.prepare_keyboard(request.user_id)
                                        )

        else:
            if (mops_class.poll_started()
                    and mops_class.is_mopping(request.user_id)):
                self.essential.send_message(request.user_id,
                                            phrases.message_finished_twice,
                                            self.essential.prepare_keyboard(request.user_id))
            else:
                self.essential.send_message(request.user_id,
                                            phrases.message_wrong_finished,
                                            self.essential.prepare_keyboard(request.user_id))

    def accept(self, request):
        """Функция, которая запускается при команде /accept
            (Во время голосования человек подтверждает, 
            что ответственный помыл полы)

        :param request: Объект запроса

        """
        mops_class = MoppingHandler()
        if mops_class.poll_started():
            if mops_class.is_mopping(request.user_id):
                self.essential.send_message(request.user_id,
                                            phrases.message_cant_vote,
                                            self.essential.prepare_keyboard(request.user_id))
                return
            if (request.user_id in mops_class.get_users_for()
                    or request.user_id in mops_class.get_users_against()):

                self.essential.send_message(request.user_id,
                                            phrases.message_voted_twice,
                                            self.essential.prepare_keyboard(request.user_id))
                return

            mops_class.vote_for(request.user_id)
            self.essential.send_message(request.user_id, phrases.message_accept
                                        .format(*self.essential.get_user_full_name(mops_class.who_is_next())),
                                        self.essential.prepare_keyboard(request.user_id))
            if mops_class.all_voted():
                self.verdict(mops_class)
        else:
            self.essential.send_message(request.user_id,
                                        phrases.message_early_accept,
                                        self.essential.prepare_keyboard(request.user_id))

    def decline(self, request):
        """Функция, которая запускается при команде /decline
            (Во время голосования человек опровергает, 
            что ответственный помыл полы)

        :param request: Объект запроса

        """

        mops_class = MoppingHandler()
        if mops_class.is_poll_started:

            if mops_class.is_mopping(request.user_id):
                self.essential.send_message(request.user_id,
                                            phrases.message_cant_vote,
                                            self.essential.prepare_keyboard(request.user_id))
                return

            if (request.user_id in mops_class.get_users_for()
                    or request.user_id in mops_class.get_users_against()):
                self.essential.send_message(request.user_id,
                                            phrases.message_voted_twice,
                                            self.essential.prepare_keyboard(request.user_id))
                return

            mops_class.vote_against(request.user_id)
            self.essential.send_message(request.user_id,
                                        phrases.message_decline.format(
                                            *self.essential.get_user_full_name(
                                                mops_class.who_is_next()
                                            )
                                        ),
                                        self.essential.prepare_keyboard(request.user_id))
            if mops_class.all_voted():
                self.verdict(mops_class)
        else:
            self.essential.send_message(request.user_id,
                                        phrases.message_early_decline,
                                        self.essential.prepare_keyboard(request.user_id))

    def verdict(self, mops_class):
        """Функция вызывается, если проголосовали все люди

        :param mops_class: Объект класса MoppingHandler

        """
        message = "Итоги голосования: \n"
        if mops_class.get_voting_result():
            mops_class.go_further()
            message += phrases.message_vote_success.format(
                *self.essential.get_user_full_name(mops_class.who_is_next()))

            mops_class.close_poll()
            for user in mops_class.get_users():
                self.essential.send_message(user,
                                            message,
                                            self.essential.prepare_keyboard(user))
        else:
            mp_infos = []
            for user in mops_class.get_users_against():
                mp_infos.append('{0} {1}'
                                .format(*self.essential.get_user_full_name(user)))
            message += "Несостыковочка! Следующие пользователи не согласны: \n"

            for mp in mp_infos:
                message += mp
                message += '\n '
            message += "Пусть {0} {1} лучше помоет полы и снова запустит команду"\
                .format(*self.essential.get_user_full_name(mops_class.who_is_next()))
            mops_class.close_poll()
            for user in mops_class.get_users():
                self.essential.send_message(
                    user, message, self.essential.prepare_keyboard(user))

    def reset(self, request):
        """ Сброс очереди на мойку полов

        :param request: Объект запроса
        """
        if self.admin.is_admin(request.user_id):
            mops_class = MoppingHandler()
            new_queue = [int(id) for id in request.message.split()[1:]]
            mops_class.reset_system(new_queue)
            self.essential.send_message(request.user_id,
                                        phrases.message_successful_reset)

            global_broadcast_message = 'Админ изменил очередь. \
                                        Следующий порядок такой: \n'
            for user in mops_class.get_users()[::-1]:
                global_broadcast_message += '{0} {1} \n'\
                    .format(*self.essential.get_user_full_name(user))
            global_broadcast_message += "P.S. Кто первый в списке, тот и моет"

            for user in mops_class.get_users():
                self.essential.send_message(user,
                                            global_broadcast_message,
                                            self.essential.prepare_keyboard(user))

        else:
            self.essential.send_message(request.user_id,
                                        phrases.message_not_admin,
                                        self.essential.prepare_keyboard(request.user_id))

    def get_queue(self, request):
        """Вывод очереди

        :param request: Объект запроса
        """
        mops_class = MoppingHandler()
        global_broadcast_message = 'Очередь выглядит следующим образом: \n'
        for user in mops_class.get_users()[::-1]:
            global_broadcast_message += '{0} {1} \n'\
                .format(*self.essential.get_user_full_name(user))

        self.essential.send_message(request.user_id,
                                    global_broadcast_message,
                                    self.essential.prepare_keyboard(request.user_id))


class AdminPanel:
    """ Управление админкой

    :param admins: Список id админов
    """

    def __init__(self):
        self.admins = [210445124]

    def is_admin(self, vk_id):
        return vk_id in self.admins


def saving(function):
    """Декоратор для сохранения изменений в файл
    """
    def wrapper(*args):
        function(*args)
        args[0].save_to_file()
    return wrapper


class MoppingHandler:
    """Класс, который следит за тем, чтобы
        ВСЕ БЛИН ВЗЯЛИ ШВАБРЫ И ПОШЛИ МЫТЬ ПОЛ

    :param queue: Очередь тех, кто ответсвенный за мойку полов. 
        Последний в очереди моет полы

    :param is_poll_started: Голосование идет?

    :param voted_for: Список тех, кто согласен с результатом мойки полов

    :param voted_against: Список тех, кто не согласен с результатом мойки полов
    """

    def __init__(self):
        self.queue_size = 0
        self.queue = []
        self.is_poll_started = False
        self.voted_for = []
        self.voted_against = []
        self.read_from_file()

    @saving
    def reset_system(self, new_queue):
        self.queue = new_queue
        self.queue_size = len(new_queue)
        self.is_poll_started = False
        self.voted_for = []
        self.voted_against = []

    @saving
    def start_poll(self):
        """ Запуск голосования
        """
        self.is_poll_started = True

    @saving
    def close_poll(self):
        """ Закрытие голосования
        """
        self.is_poll_started = False
        self.voted_for = []
        self.voted_against = []

    def get_users_for(self):
        return self.voted_for

    def get_users_against(self):
        return self.voted_against

    @saving
    def vote_for(self, user_id):
        self.voted_for.append(user_id)

    @saving
    def vote_against(self, user_id):
        self.voted_against.append(user_id)

    def get_voting_result(self):
        return len(self.voted_for) == len(self.queue)-1

    def get_users(self):
        return self.queue

    def poll_started(self):
        return self.is_poll_started

    def is_mopping(self, vk_id):
        return vk_id == self.queue[-1]

    def read_from_file(self):
        """Функция чтения из конфигурационного файла
        """
        input_str = ''
        with open("Mopping.txt", "r") as file:
            input_str = file.read().split('\n')
        cursor_pos = 0
        self.queue_size = int(input_str[cursor_pos])

        for i in range(self.queue_size):
            cursor_pos += 1
            self.queue.append(int(input_str[cursor_pos]))

        cursor_pos += 1
        self.is_poll_started = bool(int(input_str[cursor_pos]))
        cursor_pos += 1

        size_of_votes_list = int(input_str[cursor_pos])
        for i in range(size_of_votes_list):
            cursor_pos += 1
            self.voted_for.append(int(input_str[cursor_pos]))
        cursor_pos += 1
        size_of_votes_list = int(input_str[cursor_pos])
        for i in range(size_of_votes_list):
            cursor_pos += 1
            self.voted_against.append(int(input_str[cursor_pos]))

    def who_is_next(self):
        return self.queue[-1]

    @saving
    def go_further(self):
        finished_person = self.queue.pop()
        self.queue.insert(0, finished_person)

    def all_voted(self):
        return len(self.voted_for) + len(self.voted_against) == 3

    def save_to_file(self):
        """Функция записи в конфигурационный файл

        Формат Mopping.txt:

        -Размер очереди
        -Список id пользователей
        -int(self.is_poll_started)
        -len(self.voted_for)
        -Список id из voted_for
        -len(self.voted_agains)
        -Список id из voted_against

        """
        buffer = ''
        buffer += str(self.queue_size)
        buffer += '\n'
        for user_id in self.queue:
            buffer += (str(user_id) + '\n')
        buffer += (str(int(self.is_poll_started))+'\n')
        buffer += (str(len(self.voted_for)) + '\n')
        for user_id in self.voted_for:
            buffer += (str(user_id) + '\n')
        buffer += (str(len(self.voted_against)) + '\n')
        for user_id in self.voted_against:
            buffer += (str(user_id) + '\n')
        with open("Mopping.txt", "w+") as file:
            file.write(buffer)
